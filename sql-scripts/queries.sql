-- SQLite
-- 9 - Construa uma query SQL para editar o campo e-mail do cliente com nome 
--     Carolina, onde devemos trocar de “carol@ig.com.br” para “carolina@campuscode.com.br”.
UPDATE customers
SET email = 'carolina@campuscode.com.br'
WHERE email = 'carol@ig.com.br';


-- 10 - Construa uma query SQL para editar a data de nascimento do cliente com
--      nome Josefa para “1986-06-19”.
UPDATE customers AS c
SET birth_date = '1986-06-19'
WHERE c.name = 'Josefa';


-- 11 - Construa uma query SQL para editar o ano do automóvel com nome 
--      Fiat Cronos de “2022” para “2019” da tabela de CARS
UPDATE cars
SET year = 2019
WHERE name = 'Fiat Cronos';


-- 12 - Construa uma query SQL para excluir o automóvel com 
--      nome Hyundai HB20 1.6 da tabela de CARS
DELETE FROM cars
WHERE name = 'Hyundai HB20 1.6';


-- 13- Construa uma query SQL para alterar o nome da coluna 
--     “PHONE” da tabela de EMPLOYEES, para “PHONE NUMBER”
ALTER TABLE employees
RENAME COLUMN phone TO 'phone number';


-- 14 - Construa uma consulta capaz de exibir somente o 
--      name, lastname e email dos clientes que moram no estado de SP
SELECT name, lastname, email
FROM customers
WHERE state = 'SP';


-- 15 - Construa uma consulta capaz de exibir somente os automóveis 
--      que estão com o status “Liberado”
SELECT name, year, color
FROM cars
WHERE status = 'Liberado';


-- 16 - Construa uma consulta capaz de exibir todos os automóveis do ano 2016.
SELECT name, year, color
FROM cars
WHERE year = 2016;


-- 17 - Construa uma consulta capaz de exibir todos os funcionários 
-- e seus respectivos cargos
SELECT e.name, p.description 
FROM employees AS e JOIN positions AS p
WHERE e.position_id = p.id;


-- 18 - Construa uma consulta capaz de exibir somente os funcionários que 
--      realizaram mais ou igual a 2 locações.
SELECT e.name
FROM employees AS e JOIN locations AS l
WHERE e.id = l.employee_id
GROUP BY l.employee_id
HAVING COUNT(l.employee_id) > 1;


-- 19 - Construa uma consulta capaz de exibir somente os clientes que 
--      realizaram mais ou igual a 2 locações.
SELECT c.name
FROM customers AS c JOIN locations AS l
WHERE c.id = l.customer_id
GROUP BY l.customer_id
HAVING COUNT(l.customer_id) > 1;


-- 20 - Construa uma consulta capaz de exibir todas as locações realizadas,
--       assim como também o nome do cliente, do automóvel e do funcionário vinculados em cada locação
SELECT c.name AS 'Cliente', a.name AS 'Automóvel', e.name AS 'Funcionário' 
FROM locations AS l JOIN customers AS c
    JOIN cars as a JOIN employees as e
WHERE l.customer_id = c.id AND l.car_id = a.id 
        AND l.employee_id = e.id;


-- 21 - Construa uma consulta capaz de exibir quantas locações existem na tabela de LOCATIONS
SELECT COUNT(*)
FROM locations;


-- 22 - Construa uma consulta capaz de exibir qual foi a locação com o TOTAL com maior valor.
SELECT id, start_date, end_date, total
FROM locations
WHERE total = (SELECT MAX(total) FROM locations);


-- 23 - Construa uma consulta capaz de exibir todas as locações realizadas,
--       entre as datas “2022-05-20” a “2022-12-25”
SELECT id, start_date, end_date, total
FROM locations
WHERE start_date >= datetime('2022-05-20') AND
      end_date <= datetime('2022-12-25');

