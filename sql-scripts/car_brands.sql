-- SQLite
CREATE TABLE car_brands (
    id INTEGER PRIMARY KEY,
    brand_name VARCHAR(120) NOT NULL
);

INSERT INTO car_brands (brand_name) 
    VALUES ('Chevrolet'),
           ('Toyota'),
           ('Hyundai'),
           ('Volkswagen'),
           ('Jeep'),
           ('Renault'),
           ('Honda'),
           ('Fiat');

SELECT * FROM car_brands; 
