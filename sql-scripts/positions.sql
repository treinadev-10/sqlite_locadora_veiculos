-- SQLite
CREATE TABLE positions (
    id INTEGER PRIMARY KEY,
    description VARCHAR(120) NOT NULL
);

INSERT INTO positions(description)
    VALUES ('Gerente de vendas'),
           ('Gerente de compras'),
           ('Vendedor'),
           ('Mecânico'),
           ('Assistente Administrativo');
